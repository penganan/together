﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMVC
{
    public class AppSettings
    {
        public string GaoDeMapKey { get; set; }

        public string APIGatewayEndpoint { get; set; }
    }
}
