﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Together.Activity.API.Applications.Queries
{
    using Npgsql;
    using Together.Activity.API.Models;
    using Together.Activity.Infrastructure.Data;

    public class ActivityQueries
        : IActivityQueries
    {
        private string _connectionString = string.Empty;
        public ActivityQueries(string constr)
        {
            _connectionString = !string.IsNullOrWhiteSpace(constr) ? constr : throw new ArgumentNullException(nameof(constr));
        }

        public async Task<IEnumerable<ActivitySummaryViewModel>> GetActivitiesAsync(int pageIndex, int pageSize, int status = 2)
        {
            var sql = "SELECT a.\"Id\" AS \"ActivityId\", a.\"Description\" AS \"Title\", a.\"LimitsNum\", s.\"Name\" AS \"Status\", COALESCE(c.\"num\", 0) AS  \"NumberOfParticipants\" FROM \"public\".activities AS a LEFT JOIN \"public\".activitystatus AS s ON a.\"ActivityStatusId\" = s.\"Id\" LEFT JOIN(SELECT \"ActivityId\",COUNT(*) AS num FROM \"public\".participant GROUP BY \"ActivityId\") c ON a.\"Id\" = c.\"ActivityId\" WHERE a.\"ActivityStatusId\" = @status LIMIT(@limit) OFFSET(@offset)";
            return await SqlQuery<ActivitySummaryViewModel>(sql, new { limit = pageSize, offset = (pageIndex - 1) * pageSize, status });
        }

        public async Task<ActivityViewModel> GetActivityAsync(int id)
        {
            //using (var connection = new NpgsqlConnection(_connectionString))
            //{
            //    connection.Open();
            var sql = "SELECT a.\"OwnerId\", a.\"Id\" AS \"ActivityId\", a.\"Description\", a.\"Details\", a.\"CreateTime\", a.\"EndRegisterTime\", a.\"ActivityStartTime\", a.\"ActivityEndTime\", a.\"LimitsNum\", s.\"Name\" AS \"Status\", p.\"UserId\", p.\"Nickname\", p.\"JoinTime\", p.\"Avatar\", p.\"Sex\" FROM activities AS a LEFT JOIN participant AS p ON a.\"Id\" = p.\"ActivityId\" LEFT JOIN activitystatus AS s ON a.\"ActivityStatusId\" = s.\"Id\" WHERE a.\"Id\" = @Id";
            //    var result = await connection.QueryAsync<dynamic>(sql, new { id });

            var result = await SqlQuery<dynamic>(sql, new { id });
            if (result.AsList().Count <= 0)
            {
                throw new KeyNotFoundException();
            }
            return MapActivityAndParticipant(result);
            //}
        }

        public async Task<IEnumerable<ActivitySummaryViewModel>> GetActivitiesByUserAsync(string userId)
        {
            var sql = "SELECT a.\"Id\" as \"ActivityId\", a.\"Description\" as \"Title\",a.\"LimitsNum\", s.\"Name\" as \"Status\" FROM activities a LEFT JOIN activitystatus s ON a.\"ActivityStatusId\" = s.\"Id\" WHERE a.\"Id\" IN( SELECT DISTINCT \"ActivityId\" FROM participant p WHERE p.\"UserId\"=@userId)";
            return await SqlQuery<ActivitySummaryViewModel>(sql, new { userId });
        }

        private ActivityViewModel MapActivityAndParticipant(dynamic result)
        {
            var activity = new ActivityViewModel
            {
                ActivityId = result[0].ActivityId,
                Status = result[0].Status,
                OwnerId = result[0].OwnerId,
                Description = result[0].Description,
                Details = result[0].Details,
                EndRegisterTime = result[0].EndRegisterTime,
                ActivityStartTime = result[0].ActivityStartTime,
                ActivityEndTime = result[0].ActivityEndTime,
                CreateTime = result[0].CreateTime,
                //Address = result[0].Address,
                LimitsNum = result[0].LimitsNum
            };


            foreach (dynamic item in result)
            {
                if (item.UserId != null)
                {
                    activity.Participants.Add(new ParticipantViewModel
                    {
                        UserId = item.UserId,
                        Avatar = item.Avatar,
                        JoinTime = item.JoinTime,
                        Nickname = item.Nickname,
                        Sex = item.Sex
                    });
                }
            }

            return activity;
        }

        private async Task<IEnumerable<T>> SqlQuery<T>(string sql, object args)
        {
            using (var connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();
                return await connection.QueryAsync<T>(sql, args);
            }
        }
    }
}
