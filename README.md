# TOGETHER - a microservice application on docker container

none


## Urls

[MVC](http://localhost:5001)

[IdentityServer](http://localhost:5000)

[api gateway](http://localhost:8000)

[consul](http://localhost:8500)

[rabbitmq](http://localhost:15672)